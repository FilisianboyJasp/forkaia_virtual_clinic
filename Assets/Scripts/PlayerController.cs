﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed = 6;

    bool canMove;

    Rigidbody rb;
    //Renderer my_renderer;
    Renderer[] my_renderers;
    GameManagerController GameManager;
    Animator my_animator;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        //my_renderer = GetComponent<Renderer>();
        my_renderers = GetComponentsInChildren<Renderer>();
        GameManager = GameManagerController.getInstance();
        GameManager.SetPlayer(GetComponent<PlayerController>());
        canMove = true;
        my_animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (canMove)
        {
            //input
            float h = Input.GetAxis("Horizontal");
            float v = Input.GetAxis("Vertical");

            //movement
            rb.velocity = (Vector3.right * h + Vector3.forward * v) * speed;

            //facing
            if (h != 0 || v != 0)
            {
                transform.forward = Vector3.right * h + Vector3.forward * v;
                //Debug.Log("Walking");
                my_animator.SetBool("isWalking", true);
            }
            else
            {
                //Debug.Log("Idle");
                my_animator.SetBool("isWalking", false);
            }
        }
    }


    /*These should be Coroutines so everything looks smooth*/

    //function to fade out (temporary, not smoothed)
    public void FadeOut()
    {
        rb.velocity = Vector3.zero;

        if (canMove)
            canMove = false;

        //my_renderer.enabled = false;
        for (int i = 0; i < my_renderers.Length; i++)
        {
            my_renderers[i].enabled = false;
        }
    }

    //function to fade in (temporary, not smoothed)
    public void FadeIn()
    {
        if (!canMove)
            canMove = true;

        //my_renderer.enabled = true;
        for (int i = 0; i < my_renderers.Length; i++)
        {
            my_renderers[i].enabled = true;
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "End")
        {
            GameManager.DrawScoreDisplay();
            Time.timeScale = 0f;
            /*Time.timeScale = 0f; //freezes game
            GameObject scoreCanvas = GameObject.Find("ScoreCanvas");
            scoreCanvas.GetComponent<Canvas>().enabled = true;
            //DrawScoreDisplay(); // shows scoreboard*/
        }
    }

    /*void OnTriggerExit(Collider other)
    {
        if (other.transform.tag == "End")
        {
            GameManager.DrawScoreDisplay();
            Time.timeScale = 0f;
            //Time.timeScale = 0f; //freezes game
            //GameObject scoreCanvas = GameObject.Find("ScoreCanvas");
            //scoreCanvas.GetComponent<Canvas>().enabled = true;
            //DrawScoreDisplay(); // shows scoreboard
        }
    }*/
}
