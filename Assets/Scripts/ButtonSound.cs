﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonSound : MonoBehaviour
{
    public AudioSource sound;
    public AudioClip hoverSound;
    public AudioClip clickSound;


    //function plays sound when mouse hover button
    public void Hover() 
    {
        sound.PlayOneShot(hoverSound);
    }

    //function plays sound when mouse clicks button

    public void Click() 
    {
        sound.PlayOneShot(clickSound);
    }
}
