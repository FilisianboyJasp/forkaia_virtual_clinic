﻿//Jasper Natag-oy
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookatPlayer : MonoBehaviour
{
    public Transform Player;
    public float degreesPerSecond;

   void Update()
    {
        Vector3 dirFromMeToTarget = Player.position - transform.position;
        dirFromMeToTarget.y = 0.0f;
        Quaternion lookRotation = Quaternion.LookRotation(dirFromMeToTarget);
        transform.rotation =
            Quaternion.Lerp(transform.rotation, lookRotation, Time.deltaTime * (degreesPerSecond / 360.0f));
    }
}
