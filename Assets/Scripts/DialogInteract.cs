﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogInteract : MonoBehaviour
{
    public Vector3 dialogPos;
    public Vector3 lookAtPos;
    public bool dialogActive = false;
    public DialogTree dialogTree;
    public DialogInput dialogInput;
    public GameObject room;
    GameObject indicator;
    GameManagerController GameManager;
    public string defaultDialog;

    // Start is called before the first frame update
    void Start()
    {
        GameManager = GameManagerController.getInstance();

        if (room != null && dialogInput == null)
            dialogInput = room.GetComponent<DialogInput>();
        if (dialogInput == null)
            Debug.Log("!Warning! Object: " + gameObject.name + " :does not have a dialog input attached");

        indicator = GameObject.Find("Indicator");
        if (defaultDialog == null)
            defaultDialog = "";
    }

    // Update is called once per frame
    void Update()
    {
        if (dialogActive && Input.GetKeyDown(KeyCode.F))
        {
            dialogActive = false;
            GameManager.DeactivateDialog();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!dialogActive && other.CompareTag("Player"))
        {
            indicator.transform.position = new Vector3(transform.position.x, 3.5f, transform.position.z);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (!dialogActive && other.CompareTag("Player"))
        {
            indicator.transform.position = Vector3.down * 5 + Vector3.forward * 5;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (Input.GetKeyDown(KeyCode.Space) && !dialogActive && other.CompareTag("Player")
            /*&& check if other is facing us enough*/)
        {
            dialogActive = true;

            bool my_flag = true;
            int i = 0;
            if (dialogInput != null)
            {
                while (my_flag)
                {
                    dialogTree = dialogInput.dialogTrees[Random.Range(0, dialogInput.dialogTrees.Count - 1)];
                    my_flag = GameManager.CheckIfAnswered(dialogTree);
                    i++;
                    if (i > 20)
                    {
                        //go down the line and find an unaswered
                        for (int j = 0; j < dialogInput.dialogTrees.Count; j++)
                        {
                            dialogTree = dialogInput.dialogTrees[j];
                            if (!GameManager.CheckIfAnswered(dialogTree))
                            {
                                my_flag = false;
                                break;
                            }
                        }
                        break;
                    }
                }
            }
            else if (GetComponent<DialogInput>() != null)
            {
                while (my_flag)
                {
                    dialogTree = GetComponent<DialogInput>().dialogTrees[Random.Range(0, GetComponent<DialogInput>().dialogTrees.Count - 1)];
                    my_flag = GameManager.CheckIfAnswered(dialogTree);
                    i++;
                    if (i > 20)
                    {
                        //go down the line and find an unaswered
                        for (int j = 0; j < GetComponent<DialogInput>().dialogTrees.Count; j++)
                        {
                            dialogTree = GetComponent<DialogInput>().dialogTrees[j];
                            if (!GameManager.CheckIfAnswered(dialogTree))
                            {
                                my_flag = false;
                                break;
                            }
                        }
                        break;
                    }

                }
            }
            else
            {
                dialogTree = new DialogTree();
                my_flag = false;
            }

            if (my_flag)
            {
                GameManager.DefaultResponse(defaultDialog, this);
            }
            else
            {
                //use our created
                GameManager.ActivateDialog(this);
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        //Gizmos.DrawWireSphere(dialogPos+transform.position, 0.5f);
        Gizmos.DrawWireSphere(transform.TransformDirection(dialogPos) + transform.position, 0.5f);

        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(lookAtPos + transform.position, 0.5f);
    }
}
