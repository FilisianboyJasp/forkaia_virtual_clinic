﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public static bool GameIsPaused = false;

    public GameObject pauseMenuUI;

     // Update is called once per frame
    void Update()
    {
        // Checks for ESCAPE input if true calls resume function, false calls pause function
        if (Input.GetKeyDown(KeyCode.Escape)) 
        {
            if (GameIsPaused)
            {
                //Calls Resume Function
                Resume(); 
            }
            else 
            {
                //Calls Pause Function
                Pause();
            }
        }
    }

    //enables UI and resumes game
    public void Resume() 
    {
        pauseMenuUI.SetActive(false);//disables pause UI
        Time.timeScale = 1f;// resume game time
        GameIsPaused = false;
    }

    //enables UI and stops game
    void Pause() 
    {
        pauseMenuUI.SetActive(true); //enables pause UI
        Time.timeScale = 0f; // stops game time
        GameIsPaused = true;
    }

    //switches to MenuScene
    public void LoadMenu() 
    {
        SceneManager.LoadScene("MainMenu");
    }

    //exits game
    public void QuitGame() 
    {
        Debug.Log("Quit Game");
        Application.Quit();
    }

    //reloads the current scene
    public void Restart()
    {
        SceneManager.LoadScene(Application.loadedLevel);
    }
}
