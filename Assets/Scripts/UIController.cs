﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//https://answers.unity.com/questions/885612/46-gui-resizing-panel-to-fit-it-content.html

public class UIController : MonoBehaviour
{
    GameManagerController GameManager;
    GameObject messagePanel;
    Text messageText;
    ToggleGroup answerToggleGroup;
    Button submitButton;
    Button exitButton;
    Button backButton;
    Button nextButton;
    DialogTree currentDialog;
    int dialogIndex;
    Toggle[] answerToggles;
    PopUpText popUpText;

    string[] currentDialogMessages;
    string[] currentDialogAnswers;
    int currentDialogRightAnswerIndex;

    // Start is called before the first frame update
    void Start()
    {
        GameManager = GameManagerController.getInstance();
        GameManager.SetUserInterface(GetComponent<UIController>());
        messagePanel = GameObject.Find("MessagePanel");
        messageText = GameObject.Find("MessageText").GetComponent<Text>();
        messagePanel.gameObject.SetActive(false);
        answerToggleGroup = GameObject.Find("AnswerPanel").GetComponent<ToggleGroup>();
        answerToggles = GameObject.Find("AnswerPanel").GetComponentsInChildren<Toggle>();
        answerToggleGroup.gameObject.SetActive(false);
        submitButton = GameObject.Find("SubmitButton").GetComponent<Button>();
        submitButton.onClick.AddListener(SubmitButtonPress);
        submitButton.gameObject.SetActive(false);
        exitButton = GameObject.Find("ExitButton").GetComponent<Button>();
        exitButton.onClick.AddListener(ExitButtonPress);
        exitButton.gameObject.SetActive(false);
        backButton = GameObject.Find("BackButton").GetComponent<Button>();
        backButton.onClick.AddListener(BackButtonPress);
        backButton.gameObject.SetActive(false);
        nextButton = GameObject.Find("NextButton").GetComponent<Button>();
        nextButton.onClick.AddListener(NextButtonPress);
        nextButton.gameObject.SetActive(false);

        popUpText = GameObject.Find("PopUpText").GetComponent<PopUpText>();
    }

    // Update is called once per frame
    void Update()
    {
        //***Testing***
        if (Input.GetKeyDown(KeyCode.G))
        {
            GameManager.ScoreTestingDebugLog();
            GameManager.DrawScoreDisplay();
        }
    }

    public void SetDialog(DialogTree dialog)
    {
        currentDialog = dialog;
        dialogIndex = 0;
        messagePanel.gameObject.SetActive(true);
        answerToggleGroup.gameObject.SetActive(true);
        answerToggleGroup.SetAllTogglesOff();
        submitButton.gameObject.SetActive(true); submitButton.interactable = true;
        exitButton.gameObject.SetActive(true); exitButton.interactable = true;
        backButton.gameObject.SetActive(true);
        backButton.interactable = false;
        nextButton.gameObject.SetActive(true);
        nextButton.interactable = true;

        currentDialogMessages = currentDialog.messages;
        currentDialogAnswers = currentDialog.answers;
        currentDialogRightAnswerIndex = currentDialog.rightAnswerIndex;

        messageText.text = currentDialog.messages[dialogIndex];
        for (int i = 0; i < currentDialog.answers.Length; i++)
        {
            answerToggles[i].gameObject.GetComponentInChildren<Text>().text = currentDialog.answers[i];
        }

        popUpText.ClearText();
    }

    public void SetDefaultUI()
    {
        messagePanel.gameObject.SetActive(false);
        answerToggleGroup.gameObject.SetActive(false);
        submitButton.gameObject.SetActive(false); submitButton.interactable = false;
        exitButton.gameObject.SetActive(false); exitButton.interactable = false;
        backButton.gameObject.SetActive(false);
        nextButton.gameObject.SetActive(false);

        currentDialog = null;
    }

    void SubmitButtonPress()
    {
        if (!answerToggleGroup.AnyTogglesOn())
        {
            //pop up message "Please select an answer"
            popUpText.SetText("Please select an answer");
            //Debug.Log("Please select an answer");
        }
        else
        {
            if (!answerToggles[currentDialog.rightAnswerIndex].isOn)
            {
                //pop up message wrong
                popUpText.SetText("Incorrect");
                GameManager.IncorrectAnswer();
                //Debug.Log("Incorrect");
            }
            else
            {
                //pop up message right
                popUpText.SetText("Correct");
                GameManager.CorrectAnswer();
                //Debug.Log("Correct");
            }

            //add to answered questions list
            GameManager.AddToAnswered(currentDialog);

            GameManager.DeactivateDialog();
        }
    }
    void ExitButtonPress()
    {
        popUpText.ClearText();

        if (currentDialog != null)
        {
            GameManager.DeactivateDialog();
        }
        else
        {
            GameManager.DeactivateMessage();
        }
    }
    void BackButtonPress()
    {
        if (dialogIndex != 0)
        {
            dialogIndex--;
            messageText.text = currentDialogMessages[dialogIndex];
            backButton.interactable = dialogIndex == 0 ? false : true;
            nextButton.interactable = true;
        }
    }
    void NextButtonPress()
    {
        if (dialogIndex != currentDialogMessages.Length - 1)
        {
            dialogIndex++;
            messageText.text = currentDialogMessages[dialogIndex];
            nextButton.interactable = dialogIndex == currentDialogMessages.Length - 1 ? false : true;
            backButton.interactable = true;
        }
    }

    public void SetMessageDialog(DialogNonInteract dialog)
    {
        dialogIndex = 0;
        messagePanel.gameObject.SetActive(true);
        exitButton.gameObject.SetActive(true); exitButton.interactable = true;
        backButton.gameObject.SetActive(true);
        backButton.interactable = false;
        nextButton.gameObject.SetActive(true);
        nextButton.interactable = true;

        currentDialogMessages = dialog.messages.ToArray();

        messageText.text = currentDialogMessages[dialogIndex];

        popUpText.ClearText();
    }

    public void SetDefaultMessage(List<string> messages)
    {
        dialogIndex = 0;
        messagePanel.gameObject.SetActive(true);
        exitButton.gameObject.SetActive(true); exitButton.interactable = true;
        backButton.gameObject.SetActive(true);
        backButton.interactable = false;
        nextButton.gameObject.SetActive(true);
        nextButton.interactable = true;

        currentDialogMessages = messages.ToArray();

        messageText.text = currentDialogMessages[dialogIndex];

        popUpText.ClearText();

    }
}
