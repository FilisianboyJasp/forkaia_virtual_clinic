-
The U.S. Centers for Disease Control and Prevention (CDC) lists fever as one criterion for screening for 
COVID-19. In checking this patients temperature, what would register as a fever by CDC standards?
(A) 99.8 or higher
(B) 100.4 or higher
(C) 101 or higher
(D) 101.7 or higher
(E) Any of the above
(Answer: B)
-
Symptoms may appear 2-14 days after exposure to the COVID-19 virus. To help ensure this patient's health 
when collecting Anterior nasal (nares) specimen, how long should you take to collect a sample?
(A) 10 seconds, rotating twice
(B) 5 seconds, rotating fully each nostril
(C) 15 seconds, rotating at least 4 times
(D) 20 seconds, rotating at least 3 times
(E) none of the above
(Answer: C)
-
The number of expected positive COVID-19 results is low, and Pooling Procedures are being used. If this
patient is involved in the pool and the pool comes back negative, what would be an appropriate response to the
patient?
(A) Tell the patient they should be tested again
(B) Tell the patient their test came back negative
(C) Check the patients individual sample to be sure
(D) Tell the patient that they are at lower risk
(E) There is no need to contact the patient
(Answer: B)